const express = require('express')
const prometheus = require('prom-client')


const app = express()
const port = process.env.PORT || 8080

app.use(express.static('public'))
app.use(express.json())

function fancyName() {
  let adjs = ["autumn", "hidden", "bitter", "misty", "silent", "empty", "dry",
  "dark", "summer", "icy", "delicate", "quiet", "white", "cool", "spring",
  "winter", "patient", "twilight", "dawn", "crimson", "wispy", "weathered",
  "blue", "billowing", "broken", "cold", "damp", "falling", "frosty", "green",
  "long", "late", "lingering", "bold", "little", "morning", "muddy", "old",
  "red", "rough", "still", "small", "sparkling", "throbbing", "shy",
  "wandering", "withered", "wild", "black", "young", "holy", "solitary",
  "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine",
  "polished", "ancient", "purple", "lively", "nameless"]

  , nouns = ["waterfall", "river", "breeze", "moon", "rain", "wind", "sea",
  "morning", "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn",
  "glitter", "forest", "hill", "cloud", "meadow", "sun", "glade", "bird",
  "brook", "butterfly", "bush", "dew", "dust", "field", "fire", "flower",
  "firefly", "feather", "grass", "haze", "mountain", "night", "pond",
  "darkness", "snowflake", "silence", "sound", "sky", "shape", "surf",
  "thunder", "violet", "water", "wildflower", "wave", "water", "resonance",
  "sun", "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper",
  "frog", "smoke", "star"]

  return adjs[Math.floor(Math.random()*(adjs.length-1))]+"_"+nouns[Math.floor(Math.random()*(nouns.length-1))]
}

/*
    // Prometheus
    router.get("/metrics").handler { context ->
      val results = """
            # HELP error counter.
            # TYPE error gauge
            error $function_call_error_counter
            # HELP success counter.
            # TYPE success gauge
            success $function_call_success_counter
        """.trimIndent()

      context.response().putHeader("content-type", "text/plain;charset=UTF-8").end(results)
    }
*/

class GaugeComponent {
  constructor({minValue, maxValue, unit="something", name, help="tbd", startValue=0, interval=1000}) {
    let getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

    this.name = name
    this.help = help

    this.min = minValue
    this.max = maxValue
    this.unit = unit
    this.value = startValue
    this.B = Math.PI / 2
    this.unitsTranslatedToTheRight = getRandomInt(0, 5)

    this.gauge = new prometheus.Gauge({ name: this.name, help: this.help })
    this.gauge.set(this.value)
  }
  amplitude() {
    return (this.max-this.min)/2
  }
  unitsTranslatedUp() {
    return this.min + this.amplitude()
  }
  getLevel(t) {
    let level = this.amplitude() * Math.cos(this.B *(t-this.unitsTranslatedToTheRight)) + this.unitsTranslatedUp()
    return level
  }

  start() {
    return setInterval(() => {
      let now = new Date()
      let t = now.getMinutes() + now.getSeconds() / 100
      let componentValue = this.getLevel(t)
      this.gauge.set(componentValue)
      // current value
      this.value = componentValue
      //console.log("Ⓜ️ metric value[weather_temperature]", componentValue)

    }, this.interval)  

  }
}

let temperatureComponent = new GaugeComponent({
  minValue:-5.0, 
  maxValue:38.0, 
  name: 'weather_temperature', 
  help: 'temperature generator',
  intervall: 1000
})

temperatureComponent.start()

app.get('/api/weather', (req, res) => {
  res.json({
    temperature: temperatureComponent.value
  })
})

app.get('/metrics', (req, res) => {
  res.set('Content-Type', prometheus.register.contentType)
  res.send(prometheus.register.metrics())
})

let fancy_name = fancyName()

app.get('/api/hello', (req, res) => {
	res.send({
		message: `👋 Hello world 🌍`,
		pod: fancy_name
	})
})

app.listen(port, () => console.log(`🌍 webapp is listening on port ${port}!`))

